This repository contains the code, which i submitted to the company named OCULAI as a part of my technical interview.
The aim of the code is to create region of interests on given pictures depending on the activity of workers on a construction site.
DISCLAIMER: This code is released upon taking the approval of the CEO of the company during the interview. A few files have been removed to respect the confidential information, which is in the possession of the company.

This read me walks you through the code and explains it content-wise, what it does step by step. To run and achieve the same results, simply run all the cells of the notebook.

The directory structure is as follows:

coding_challange_Ali_Oguz_Can
	.
	.-->Data: The data, that was provided by Oculai.
	.
	.-->script.ipynb: The jupyter notebook, that was used to create the outputs.
	.
	.-->task.pdf: The description of the task.
	.
	.-->Readme: A basic introduction to the project.
	.
	.-->Results: The output folder, where all the pictures/ROIs etc. are stored:
		.
		.--> Created images: This folder contains all the images, that were captured off of the video using a function. --------->
		.    In here, there is a folder for each hour, where respective frames are stored.					    |
		.
		.--> Cropped ROI: This folder contains all the ROIs in a cropped format without the rest of the image. -----------------> | ------> Given Images: This folder contains the results for the originally given images.
		.			
		.--> ROI: This folder contains the original images with a rectangle on it defining the ROI. They are stored with -------> | ------> Created Images: This folder contains the results of for the "augmented" images.
		.    the intention of clearness											   
		.														    |
		.--> Scatter plots: This folder contains scatter plots of the classified x_/y_centers respectively for each hour. ------->|


1) Working on the given images:
    . Paths are defined
    . Variables are defined
    . The x_/y_center values are read, converted into pixel values and placed in a scatter plot to see the distribution of each hour. The mean value is calculated to act as the center of the ROI.
    . Weighted mean values are calculated for x_/y_centers. The weighting depends on how confident the classification algorithm detected the object belonging to the class 0 (workers).
    This means that not the every x_/y_center contribute to the mean at the same level but rather in ratio of the respective confidence level. This should balance out the uncertainty
    of the classification algorithm used in detecting the objects and deliver a more uniquely distributed result.
    . Using the weighted x/y pixel values, a ROI of size 900x600 is defined around the mean values. Two versions of ROI are stored: 1) Cropped: More focused viewing and more efficient to store 2) Full ROI: A better understanding of surroundings and
    positioning in the whole image.

2) Working on the video and creating a "new" data set:
    . Paths are defined
    . Variables are defined
    . A function, which saves a specific number(= 5 for testing purposes) of frames per hour.
    . Depending on the newly augmented dataset, the weighted means are re-calculated.
    . Again a single ROI is defined for the 5 images per hour and saved in different formats as for the original images.


Improvement opportunities:
    . The entries on the scatter plots are classified using a k-means approach, where k has been chosen as 3. Even though this function has been defined and implemented in the Jupiter notebook, I decided not to use it, as I could not really find a reasoning why one would want to classify the entries. Nevertheless, I am leaving it in the code.
    . Another function has been implemented, which creates a black/white version of the ROIs. This would reduce the number of channels and lead to less required storage space. This function could be combined with the cropped version of ROI to further lessen the space required to store. If a post/processing in the means of DNN/CNN is planned to be used on the ROIs afterwards, the cropped black/white ROI would make the DNN/CNN even faster and less computationally challenging, simply due to the fact that the input size is smaller.


I hereby declare that I have written the script by myself with the aid of opencv documentation and stackoverflow entries.
Ali Oğuz Can